Module allows to edit title, description and set up placeholder for text elements in each form. Just click on the
description link (if it is allowed in admin/config/field-tools/settings) and set up your settings for this element.
Form fields settings are related to string which consist of form_name@elemen_parent